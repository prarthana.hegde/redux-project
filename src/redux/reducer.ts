import { AppEvents } from "../events/events";
import { IAction } from "../actions/action";

const initState:IState = {
    count:0
}

export interface IState {
    count:number
}

export const reducer=(state:IState=initState,action:IAction) => {
    switch(action.type) {
        case AppEvents.SET_COUNT:
            return {...state,count:action.payload++}

        default:
            return state;
    }
}
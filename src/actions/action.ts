import { AppEvents } from "../events/events"

export interface IAction {
    type:AppEvents,
    payload:any
}

export const SetCount = (payload:any)=> {
    return {
        type:AppEvents.SET_COUNT,
        payload
    }
}


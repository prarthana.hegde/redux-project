import React  from 'react';
import logo from './logo.svg';
import './App.css';


interface IProps {
    count:number,
    onClick():void
}

export const App:React.FunctionComponent<IProps> = ({count,onClick})=> <div>
    <h2>Random Counter:{count}</h2>
    <button onClick={onClick}>Click</button>
</div>




export default App;

import {App as Component} from './App';
import { compose } from 'redux';
import {connect} from 'react-redux'; 
import {Dispatch} from 'react';
import {SetCount,IAction} from '../actions/action';

const mapStateToProps = (state:any) => {
    return {
        count:state.app.count
    };
};

const mapDispatchToProps = (dispatch:Dispatch<IAction>) => {
    return {
        onClick: ()=>dispatch(SetCount(Math.floor(Math.random() * 10000)))
    };
};

export const App = compose(connect(mapStateToProps,mapDispatchToProps))(Component);
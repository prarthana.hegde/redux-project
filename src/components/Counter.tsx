import React from 'react';
import {useDispatch,useSelector} from "react-redux";
 import { RootState } from '../services/store';
import {CounterActionTypes} from "../services/Counter_Actions";




function Counter(){

    const dispatch = useDispatch();
    const {counterNumber} = useSelector((state:any) => state.Counter);

    return (
        <div className="App">
            <h1>{counterNumber}</h1>
            <button
                onClick={(e) => {
                    dispatch({type: CounterActionTypes.SumCounter})
                }}>
               Increment
            </button>
            &nbsp;
            <button
                onClick={(e) => {
                    dispatch({type: CounterActionTypes.SubtractCounter})
                }}>
                Decrement
            </button>
        </div>
    );
}

export default Counter;


import React from 'react';
import './App.css';

import Counter from "../src/components/Counter";
import FunFact from "../src/components/FunFact";

function App() {
    return (
        <div>
            <Counter/>
            <br/>
            <FunFact/>
        </div>
    );
}

export default App;